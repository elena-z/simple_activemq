package com.nixsolutions.apachemq;

import javax.jms.JMSException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MainApp {
	private static final Logger LOGGER = LogManager.getLogger(MainApp.class.getName());

	public static void main(String[] args) throws Exception {
		try (Producer producer = new Producer()) {
			try (Consumer consumer = new Consumer()) {			
				producer.create("myfirst.q");
				consumer.create("myfirst.q");
				producer.sendMessage("Test Message");
				String test = consumer.getMessage(1000);
			}
		} catch (JMSException e) {
			e.printStackTrace();
			LOGGER.debug("Failed to send message!");
		}
	    
		CamelService service = new CamelService();
		
		for (int i = 0; i < 10; i++) {
			service.send("Test Camel Message: " + (i + 1));
		}
		Thread.sleep(1000);		
		service.stop();
	}
		
}
