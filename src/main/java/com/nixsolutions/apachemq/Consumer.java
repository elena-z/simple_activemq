package com.nixsolutions.apachemq;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Consumer implements AutoCloseable {

	private static final Logger LOGGER = LogManager.getLogger(Consumer.class.getName());

	private static String NO_MESSAGE = "no message";

	private Connection connection;
	private Session session;
	private MessageConsumer messageConsumer;

	public void create(String destinationName) throws JMSException {

		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);
		connection = connectionFactory.createConnection();
		session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		Destination destination = session.createQueue(destinationName);
		messageConsumer = session.createConsumer(destination);
		connection.start();
	}

	public void close() throws JMSException {
		connection.close();
	}

	public String getMessage(int timeout) throws JMSException {
		String text = NO_MESSAGE;
		Message message = messageConsumer.receive(timeout);
		if (message != null) {
			TextMessage textMessage = (TextMessage) message;
			text = textMessage.getText();
			LOGGER.debug("consumer received message with text='{}'", text);
		} else {
			LOGGER.debug("consumer received no message");
		}

		return text;
	}
}