package com.nixsolutions.main;

import javax.jms.JMSException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nixsolutions.activemq.Consumer;
import com.nixsolutions.activemq.Producer;
import com.nixsolutions.camel.CamelService;
import com.nixsolutions.cassandra.CassandraConnector;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class MainApp implements Job {
	private static final Logger LOGGER = LogManager.getLogger(MainApp.class.getName());

	public static void main(String[] args) throws Exception {
		// myfirst.q
		try (Producer producer = new Producer()) {
			try (Consumer consumer = new Consumer()) {
				producer.create("myfirst.q");
				consumer.create("myfirst.q");
				producer.sendMessage("Test Message");
				String test = consumer.getMessage(1000);
			}
		} catch (JMSException e) {
			e.printStackTrace();
			LOGGER.debug("Failed to send message!");
		}

		CassandraConnector client = new CassandraConnector();
		String ipAddress = args.length > 0 ? args[0] : "localhost";
		int port = args.length > 1 ? Integer.parseInt(args[1]) : 9042;
		System.out.println("Connecting to IP Address " + ipAddress + ":" + port + "...");
		client.connect(ipAddress, port);

		client.createSchema();
		client.loadData();
		String queryResult = client.querySchema();
		System.out.println(queryResult);
		client.close();

		// mycamel.q
		CamelService service = new CamelService();
		service.send(queryResult);
		Thread.sleep(1000);
		service.stop();

	}

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		CassandraConnector client = new CassandraConnector();
		client.connect("localhost", 9042);

		client.createSchema();
		client.loadData();
		String queryResult = client.querySchema();
		System.out.println(queryResult);
		client.close();

		// mycamel.q
		try {
			CamelService service = new CamelService();
			service.send(queryResult);
			Thread.sleep(1000);
			service.stop();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
