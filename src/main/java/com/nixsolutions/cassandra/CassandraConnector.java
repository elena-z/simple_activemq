package com.nixsolutions.cassandra;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Host;
import com.datastax.driver.core.Metadata;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

public class CassandraConnector {
	private Cluster cluster;
	private Session session;

	public void connect(String node, int port) {
		cluster = Cluster.builder().addContactPoint(node).withPort(port).build();
		session = cluster.connect();
	}

	public Session getSession() {
		return session;
	}

	public void createSchema() {
		session.execute(
				"CREATE KEYSPACE IF NOT EXISTS training WITH replication " + "= {'class':'SimpleStrategy', 'replication_factor':3};");
		session.execute("CREATE TABLE IF NOT EXISTS training.terms (" + "term_id uuid PRIMARY KEY," + "term_name text" + ");");
	}

	public void loadData() {
		session.execute("INSERT INTO training.terms (term_id, term_name) " + "VALUES ("
				+ "756716f7-2e54-4715-9f00-91dcbea6cf50," + "'Autumn-2014'" +  ");");
		session.execute("INSERT INTO training.terms (term_id, term_name) " + "VALUES ("
				+ "2cc9ccb7-6221-4ccb-8387-f22b6a1b354d," + "'Spring-2015'" + ");");
	}

	public String querySchema() {
		String result="";
		ResultSet results = session
				.execute("SELECT * FROM training.terms " + "WHERE term_id = 2cc9ccb7-6221-4ccb-8387-f22b6a1b354d;");
		result = String.format("%-30s\n%s", "term_name",
				"-------------------------------");
		for (Row row : results) {
			result = result + String.format("\n%-30s", row.getString("term_name"));
		}
		return result;
	}

	public void close() {
		cluster.close();
	}

}
