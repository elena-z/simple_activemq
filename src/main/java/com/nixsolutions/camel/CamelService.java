package com.nixsolutions.camel;

import static org.apache.activemq.camel.component.ActiveMQComponent.activeMQComponent;

import javax.jms.JMSException;

import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

import com.nixsolutions.activemq.Producer;

public class CamelService {
	private static final String IN_ENDPOINT = "activemq:queue:mycamel.q";
	//private static final String OUT_ENDPOINT = "cql:localhost/simplex";
	private static final String OUT_ENDPOINT = "file://test";

	private CamelContext context;
	//private ProducerTemplate producer;
	private Producer prod;
	
	//private String sendMessage="Hello";

	public CamelService() throws Exception {
		context = new DefaultCamelContext();
		context.addRoutes(new RouteBuilder() {
			@Override
			public void configure() throws Exception {
				from(IN_ENDPOINT).to(OUT_ENDPOINT);
			}
		});

		context.addComponent("activemq", activeMQComponent("tcp://localhost:61616"));

		//producer = context.createProducerTemplate();
		prod = new Producer();
		prod.create("mycamel.q");		
		context.start();
	}

	public void send(final String message) throws JMSException {
		//producer.sendBody(IN_ENDPOINT, message);
		prod.sendMessage(message);
	}

	public void stop() throws Exception {
		context.stop();
	}

}