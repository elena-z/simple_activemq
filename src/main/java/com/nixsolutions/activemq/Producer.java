package com.nixsolutions.activemq;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Producer implements AutoCloseable {

	private static final Logger LOGGER = LogManager.getLogger(Producer.class.getName());

	private Connection connection;
	private Session session;
	private MessageProducer messageProducer;

	public void create(String destinationName) throws JMSException {

		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);
		connection = connectionFactory.createConnection();
		session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		Destination destination = session.createQueue(destinationName);
		messageProducer = session.createProducer(destination);
	}

	public void close() throws JMSException {
		connection.close();
	}

	public void sendMessage(String message) throws JMSException {
		TextMessage textMessage = session.createTextMessage(message);
		messageProducer.send(textMessage);
		LOGGER.debug("producer sent message with text='{}'", textMessage.getText());
	}
}